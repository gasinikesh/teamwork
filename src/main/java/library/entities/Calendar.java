package library.entities;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Calendar {
	
	private static Calendar self; 					/* Changed sElF to self */
	private static java.util.Calendar calendar; 	/* Changed cAlEnDaR to calendar*/
	
	
	private Calendar() {
		calendar = java.util.Calendar.getInstance();		/* Changed cAlEnDaR to calendar*/
	}
	
	public static Calendar getInstance() {				/* Renamed gEtInStAnCe to getInstance*/
		if (self == null) {			/* Changed sElF to self */
			self = new Calendar();			/* Changed sElF to self */
		}
		return self;			/* Changed sElF to self */
	}
	
	public void incrementDate(int days) {
		calendar.add(java.util.Calendar.DATE, days);		/* Changed cAlEnDaR to calendar*/
	}
	
	public synchronized void setDate(Date date) {			/* Renamed method SeT_DaTe to setDate and args DaTe to date*/
		try {
			calendar.setTime(date);								/* Changed cAlEnDaR to calendar and DaTe to date*/
	        calendar.set(java.util.Calendar.HOUR_OF_DAY, 0);  	/* Changed cAlEnDaR to calendar*/
	        calendar.set(java.util.Calendar.MINUTE, 0);  		/* Changed cAlEnDaR to calendar*/
	        calendar.set(java.util.Calendar.SECOND, 0);  		/* Changed cAlEnDaR to calendar*/
	        calendar.set(java.util.Calendar.MILLISECOND, 0);	/* Changed cAlEnDaR to calendar*/
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}	
	}
	public synchronized Date getDate() {					/* Changed method gEt_DaTe to getDate*/
		try {
	        calendar.set(java.util.Calendar.HOUR_OF_DAY, 0);  	/* Changed cAlEnDaR to calendar*/
	        calendar.set(java.util.Calendar.MINUTE, 0);  		/* Changed cAlEnDaR to calendar*/
	        calendar.set(java.util.Calendar.SECOND, 0);  		/* Changed cAlEnDaR to calendar*/
	        calendar.set(java.util.Calendar.MILLISECOND, 0);	/* Changed cAlEnDaR to calendar*/
			return calendar.getTime();							/* Changed cAlEnDaR to calendar*/
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}	
	}

	public synchronized Date getDueDate(int loanPeriod) {		/* Changed gEt_DuE_DaTe to getDueDate*/
		Date now = getDate();									/* Changed method gEt_DaTe to getDate && variable nOw to now*/
		calendar.add(java.util.Calendar.DATE, loanPeriod);		/* Changed cAlEnDaR to calendar*/
		Date dueDate = calendar.getTime();						/* Changed cAlEnDaR to calendar && dUeDaTe to dueDate*/
		calendar.setTime(now);									/* Changed cAlEnDaR to calendar && nOw to now*/
		return dueDate;											/* Changed dUeDaTe to dueDate*/
	}
	
	public synchronized long getDaysDifference(Date targetDate) {		/* Changed method GeT_DaYs_DiFfErEnCe to getDaysDifference*/
		
		long Diff_Millis = getDate().getTime() - targetDate.getTime();	/* Changed method gEt_DaTe to getDate*/
	    long Diff_Days = TimeUnit.DAYS.convert(Diff_Millis, TimeUnit.MILLISECONDS);
	    return Diff_Days;
	}

}