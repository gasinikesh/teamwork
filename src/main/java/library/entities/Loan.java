package library.entities;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class Loan implements Serializable {
	
    public static enum LoanState { CURRENT, OVER_DUE, DISCHARGED }; /* Change loandState to LoanState --Nikesh*/
    private int loanId;    /* Change LoAn_Id to loanId --Nikesh*/
    private Book book;      /* Change BoOk to book --Nikesh*/
    private Member member;  /* Change MeMbEr to member --Nikesh*/
    private Date date;      /* Change DaTe to date --Nikesh */
    private LoanState state;   /* Change lOaN_sTaTe to LoanState and StAtE to state --Nikesh */

    public Loan(int loanId, Book book, Member member, Date dueDate) {  /* Changes bOoK to book, mEmBeR to member and DuE_dAtE to dueDate --Nikesh */
        this.loanId = loanId;  /* Change LoAn_Id to loanId --Nikesh */
        this.book = book;   /* Change BoOk to book and bOoK to book --Nikesh */
        this.member = member;   /* Change MeMbEr to member and mEmBeR to member --Nikesh */
        this.date = dueDate;   /* Change DaTe to date and DuE_dAtE to dueDate --Nikesh */
        this.state = LoanState.CURRENT;    /* Change lOaN_sTaTe to LoanState and StAtE to state --Nikesh */
    }

    public void checkOverDue() {  /* Change cHeCk_OvEr_DuE to checkOverDue --Nikesh */
        if (state == LoanState.CURRENT &&  /* Change lOaN_sTaTe to LoanState and StAtE to state --Nikesh */
            Calendar.getInstance().getDate().after(date)) { /* Change gEtInStAnCe to getInstance and gEt_DaTe to getDate --Nikesh */
            this.state = LoanState.OVER_DUE;     /* Change lOaN_sTaTe to LoanState and StAtE to state --Nikesh */		
        }
    }

    public boolean isOverDue() {  /* Change Is_OvEr_DuE to isOverDue --Nikesh */
        return state == LoanState.OVER_DUE;    /* Change StAtE to state and lOaN_sTaTe to LoanState --Nikesh */	
    }

    public Integer getId() {   /* Change GeT_Id to getId --Nikesh */
        return loanId;     /* Change LoAn_Id to loanId --Nikesh */
    }

    public Date getDueDate() {    /* Change GeT_DuE_DaTe to getDueDate --Nikesh */
        return date;    /* Change DaTe to date --Nikesh */
    }

    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");  /* Change sdf to simpleDateFormat --Nikesh */
        StringBuilder stringBuilder = new StringBuilder();     /* Change sb to stringBuilder --Nikesh */
        stringBuilder.append("Loan:  ").append(loanId).append("\n")    /* Change sb to stringBuilder and LoAn_Id to loanId --Nikesh */
            .append("  Borrower ").append(member.getId()).append(" : ")  /* Change GeT_ID to getId --Nikesh*/
            .append(member.getLastName()).append(", ").append(member.getLastName()).append("\n") /* Change MeMbEr to member and GeT_LaSt_NaMe to getLastName --Nikesh*/
            .append("  Book ").append(book.getId()).append(" : " )    /* Change gEtId to getId --Nikesh */
            .append(book.getTitle()).append("\n")     /* Change BoOk to book and gEtTiTlE to getTitle --Nikesh */
            .append("  DueDate: ").append(simpleDateFormat.format(date)).append("\n")  /* Change sdf to simpleDateFormat and DaTe to date --Nikesh */
            .append("  State: ").append(state);       /* Change StAtE to state --Nikesh */
        return stringBuilder.toString();   /* Change sb to stringBuilder --Nikesh */
    }

    public Member getMember() {    /* Change GeT_MeMbEr to getMember --Nikesh */
        return member;      /* Change MeMbEr to member --Nikesh*/
    }

    public Book getBook() {    /* Change GeT_BoOk to getBook --Nikesh */
        return book;        /* Change BoOk to book --Nikesh */
    }

    public void discharge() {   /* Change DiScHaRgE to discharge --Nikesh */
        state = LoanState.DISCHARGED;             /* Change StAtE to state and lOaN_sTaTe to LoanState --Nikesh */	
    }
}

