
package library.entities;
import java.io.Serializable;

@SuppressWarnings("serial")
public class Book implements Serializable {
	
    private String title; /* Change tItLe to title --Nikesh*/
    private String author; /* Change AuThOr to author --Nikesh*/
    private String callNo;  /* Change CALLNO to callNo --Nikesh*/
    private int id; /* Change iD to id --Nikesh*/
    private enum State { AVAILABLE, ON_LOAN, DAMAGED, RESERVED };   /* Change sTate to State --Nikesh*/
    private State state; /* Change sTaTe to state and StAte to state --Nikesh*/

    public Book(String author, String title, String callNo, int id) {
        this.author = author; /* Change AuThOr to author in constructor --Nikesh*/
        this.title = title; /* Change tItLe to title on constructor --Nikesh*/
        this.callNo = callNo; /* Change CALLNO to callNo in constructor --Nikesh*/
        this.id = id;   /* Change iD to id in constructor --Nikesh*/
        this.state = State.AVAILABLE; /* Change sTaTe to State and StAte to state --Nikesh*/
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Book: ").append(id).append("\n") /* Change iD to id in toString method--Nikesh*/
            .append("  Title:  ").append(title).append("\n") /* Change tItLe to title on toString method --Nikesh*/
            .append("  Author: ").append(author).append("\n")  /* Change AuThOr to author in toString method --Nikesh*/
            .append("  CallNo: ").append(callNo).append("\n") /* Change CALLNO to callNo in toString method--Nikesh*/
            .append("  State:  ").append(state);  /* Change StAte to state in toString method--Nikesh*/
        return sb.toString();
    }

    public Integer getId() {    /* Change gEtId to getId --Nikesh*/
        return id;  /* Change iD to id in id getter method--Nikesh*/
    }

    public String getTitle() {  /* Change gEtTiTlE to getTitle --Nikesh*/
        return title; /* Change tItLe to title on title getter method --Nikesh*/
    }

    public boolean isAvailable() {     /* Change iS_AvAiLaBlE to isAvailable --Nikesh*/
        return state == State.AVAILABLE;    /* Change StAte to state and sTaTe to State --Nikesh*/
    }

    public boolean isOnLoan() {    /* Change iS_On_LoAn to isOnLoan --Nikesh*/
        return state == State.ON_LOAN;  /* Change StAtE to state and sTaTe to State --Nikesh*/
    }

    public boolean isDamaged() {   /* Change iS_DaMaGeD to isDamaged --Nikesh*/
        return state == State.DAMAGED;   /* Change StAtE to state and sTaTe to State in iS_DaMaGeD method--Nikesh*/
    }

    public void borrow() {      /* Change BoRrOw to borrow --Nikesh*/
        if (state.equals(State.AVAILABLE)) {   /* Change StAtE to state and sTaTe to State in BoRroW method--Nikesh*/
            state = State.ON_LOAN;  /* Change StAtE to state and sTaTe to State in BoRroW method--Nikesh*/
        } else { 
            throw new RuntimeException(String.format("Book: cannot borrow while book is in state: %s", state)); /* Change StAtE to state in new exception--Nikesh*/
        }
    }

    public void returnState(boolean damaged) {   /* Change ReTuRn to returnState and DaMaGed to damaged */
        if (state.equals(State.ON_LOAN)) {    /* Change StAtE to state and sTaTe to State in ReTuRn method--Nikesh*/
            if (damaged) {    /* Change DaMaGed to damaged */
                state = State.DAMAGED;    /* Change StAtE to state and sTaTe to State in ReTuRn method--Nikesh*/			
            } else {
                state = State.AVAILABLE;        /* Change StAtE to state and sTaTe to State in ReTuRn method--Nikesh*/				
            }
        } else { 
            throw new RuntimeException(String.format("Book: cannot Return while book is in state: %s", state));         /* Change StAtE to state in else condition--Nikesh*/		
        }
    }

    public void repair() {  /* Change RePaIr to repair */
        if (state.equals(State.DAMAGED)) { /* Change StAtE to state and sTaTe to State in RePaIr method if condition--Nikesh*/   
            state = State.AVAILABLE;    /* Change StAtE to state and sTaTe to State inside RePaIr method--Nikesh*/
        } else {
            throw new RuntimeException(String.format("Book: cannot repair while book is in state: %s", state)); /* Change StAtE to state in new exception--Nikesh*/
        }
    }
}

