package library.entities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Library implements Serializable {
	
    private static final String LIBRARYFILE = "library.obj";    /*Change to LIBRARYFILE -Mikesh*/
    private static final int LOANLIMIT = 2;                     /*Change to LOANLIMIT -Mikesh*/
    private static final int LOANPERIOD = 2;                    /*Change to LOANPERIOD -Mikesh*/
    private static final double FINEPERDAY = 1.0;             /*Change to FINEPERDAY -Mikesh*/
    private static final double MAXFINESOWED = 1.0;             /*Change to MAXFINESOWED -Mikesh*/
    private static final double DAMAGEFREE = 2.0;               /*Change to DAMAGEFREE -Mikesh*/
    private static Library self;                                /*Change SeLF to self -Mikesh*/
    private int bookId;                                        /*Change bOoK_Id to bookID -Mikesh*/
    private int memberId;                                       /*Change mEmBeR_iD to memberId -Mikesh*/
    private int loanId;                                         /*Change lOaN_Id to loanId*/
    private Date loanDate;                                      /*Change lOaN_DaTe to loanDate*/
    private Map<Integer, Book> catalog;                         /*Change CaTaLoG to catalog*/
    private Map<Integer, Member> members;                       /*Change MeMbErS to members*/
    private Map<Integer, Loan> loans;                           /*Change LoAnS to loans*/
    private Map<Integer, Loan> currentLoans;                   /*Change CuRrEnT_LoAnS to currentLoans*/
    private Map<Integer, Book> damagedBooks;                   /*Change DaMaGeD_BoOkS to damagedBooks*/
	

    private Library() {
        catalog = new HashMap<>();                          /*Change CaTaLoG to catalog*/
	members = new HashMap<>();                          /*Change MeMbErS to members*/
	loans = new HashMap<>();                            /*Change LoAnS to loans*/
	currentLoans = new HashMap<>();                    /*Change CuRrEnT_LoAnS to currentLoans*/
	damagedBooks = new HashMap<>();                    /*Change DaMaGeD_BoOkS to damagedBooks*/
	bookId = 1;                              /*Change book_ID to bookId*/
	memberId = 1;                            /*Change mEmBeR_Id to memberId*/		
	loanId = 1;                             /*Change lOaN_Id to loanId -Mikesh*/	
    }

	
    public static synchronized Library getInstance() {                  /*Change GeTiNsTaNcE to getInstance*/
	if (self == null) {                                         /*Change SeLf to self*/
            Path PATH = Paths.get(LIBRARYFILE);	/*Change to LIBRARYFILE -Mikesh*/		
		if (Files.exists(PATH)) {	
			try (ObjectInputStream libraryFile = new ObjectInputStream(new FileInputStream(LIBRARYFILE));) {   /*Change to LIBRARYFILE -Mikesh*/    /*Change Library_File to libraryFile*/
		            self = (Library) libraryFile.readObject();             /*Changed SeLf to self*/        /*Change LiBrArY_FiLe to libraryFile*/
                            Calendar.getInstance().setDate(self.loanDate);           /*Change SeLf to self*/              /*Change GeTiNsTaNcE to getInstance*/   /*Change SeT_DaTe to setDate*/                 /*Change lOaN_DaTe to loanDate*/
                            libraryFile.close();                                       /*Change LiBrArY_FiLe to libraryFile*/
                            }
                        catch (Exception e) {
                            throw new RuntimeException(e);
			}
		}
		else self = new Library();                              /*Change SeLf to self*/
	}
            return self;
    }

	
    public static synchronized void save() {                                /*Change SaVe to save*/
	if (self != null) {                                             /*Change SeLf to self*/
            self.loanDate = Calendar.getInstance().getDate();            /*Change gEt_DaTe to getDate*/            /*Change SeLf to self*/          /*Change GeTiNsTaNcE to getInstance*/            /* Change lOaN_DaTe to loanDate*/
		try (ObjectOutputStream libraryFile = new ObjectOutputStream(new FileOutputStream(LIBRARYFILE));) {    /*Change to LIBRARYFILE -Mikesh*/       /*Change LiBrArY_FiLe to libraryFile*/
                    libraryFile.writeObject(self);                                 /*Change SeLf to self*/     /*Change LiBrArY_FiLe to Library_File*/
                    libraryFile.flush();                                           /*Change LiBrArY_FiLe to libraryFile*/
                    libraryFile.close();                                           /*Change LiBrArY_FiLe to libraryFile*/
		}
		catch (Exception e) {
                    throw new RuntimeException(e);
		}
	}
    }

    public int getBookId() {                               /*Change get_bookId to getBookId*/
	return bookId;                                 /*Change bOoK_Id to bookId */
    }
		
    public int getMemberId() {                            /*Change gEt_MeMbEr_Id to getMemberId*/
	return memberId;                                /*Change mEmBeR_Id to memberId*/
    }
		
    private int getNextBookId() {                        /*Change gEt_NeXt_BoOk_Id to getNextBookId*/
	return bookId++;                                /*Change bOoK_Id to bookId */
    }

    private int getNextMemberId() {                      /*Change gEt_NeXt_MeMbEr_Id to getNextMemberId*/
	return memberId++;                              /*Change mEmBeR_Id to memberId*/
    }

    private int getNextLoanId() {                        /*Change gEt_NeXt_LoAn_Id to getNextLoanId*/      
	return loanId++;                                /*Change lOaN_Id to loanId*/
    }
	
    public List<Member> listMembers() {                    /*Change lIsT_MeMbErS to listMembers*/
	return new ArrayList<Member>(members.values());                 /*Change MeMbErS to members*/
    }

    public List<Book> listBooks() {                        /*Change lIsT_BoOkS to listBooks*/
	return new ArrayList<Book>(catalog.values());                   /*Change CaTaLoG to catalog*/
    }

    public List<Loan> listCurrentLoans() {                /*Change lISt_CuRrEnT_LoAnS to listCurrentLoans*/
	return new ArrayList<Loan>(currentLoans.values());             /*Change CuRrEnT_LoAnS to currentLoans*/
    }

    public Member addMember(String lastName, String firstName, String email, int phoneNo) {                /*Change aDd_MeMbEr to addMember*/		
	Member member = new Member(lastName, firstName, email, phoneNo, getNextMemberId());          /*Change gEt_NeXt_MeMbEr_Id to getNextMemberId*/
	members.put(member.getId(), member);                               /*Change MeMbErS to members*/       /*Change GeT_ID to getId*/
	return member;
    }

    public Book addBook(String a, String t, String c) {                    /*Change aDd_BoOk to addBook*/
	Book b = new Book(a, t, c, getNextBookId());                 /*Change gEt_NeXt_BoOk_Id to getNextBookId*/
	catalog.put(b.getId(), b);                                      /*Change CaTaLoG to catalog*/               /*Change gEtId to getId*/
	return b;
    }

    public Member getMember(int memberId) {                                /*Change gEt_MeMbEr to getMember */
	if (members.containsKey(memberId))                              /*Change MeMbErS to members*/
            return members.get(memberId);                           /*Change MeMbErS to members*/
	return null;
    }
	
    public Book getBook(int bookId) {                                      /*Change gEt_BoOk to getBook*/
	if (catalog.containsKey(bookId))                                /*Change CaTaLoG to catalog*/
            return catalog.get(bookId);                             /*Change CaTaLoG to catalog*/
	return null;
    }
	
    public int getLoanLimit() {                                           /*Change gEt_LoAn_LiMiT to getLoanLimit*/
		return LOANLIMIT;                                               /*Change lOaNlImIt to LOANLIMIT*/
	}
	
    public boolean canMemberBorrow(Member member) {                       /*Change cAn_MeMbEr_BoRrOw to canMemberBorrow*/
	if (member.getNumberOfCurrentLoans() == LOANLIMIT )         /*Change lOaNlImIt to LOANLIMIT*/       /*Change gEt_nUmBeR_Of_CuRrEnT_lOaNs to getNumberOfCurrentLoans*/
            return false;
			
	if (member.finesOwned() >= MAXFINESOWED)                        /*Change FiNeS_OwEd to finesOwned*/     /*Change maxFinesOwed to MAXFINESOWED*/
            return false;
				
	for (Loan loan : member.getLoans())                            /*Change GeT_LoAnS to getLoans*/
            if (loan.isOverDue())                                 /*Change Is_OvEr_DuE to isOverDue*/
            return false;
			
	return true;
    }
	
    public int getNumberOfLoansRemainingForMember(Member member) {		/*Change gEt_NuMbEr_Of_LoAnS_ReMaInInG_FoR_MeMbEr to getNumberOfLoansRemainingForMember*/ /*Change MeMbEr to member*/
	return LOANLIMIT - member.getNumberOfCurrentLoans();                /*Change MeMbEr.gEt_nUmBeR_Of_CuRrEnT_lOaNs to member.getNumberOfCurrentLoans*/
    }
	
    public Loan issueLoan(Book book, Member member) {                                  /*Change iSsUe_LoAn to issueLoan*/
	Date dueDate = Calendar.getInstance().getDueDate(LOANPERIOD);             /*Change GeTiNsTaNcE to getInstance*/       /*Change gEt_DuE_DaTe to getDueDate*/     /*Change loanPeriod to LOANPERIOD*/
	Loan loan = new Loan(getNextLoanId(), book, member, dueDate);               /*Change get_next_loan_id to getNextLoanId*/
	member.takeOutLoan(loan);                                     /*Change TaKe_OuT_LoAn to takeOutLoan*/
	book.borrow();                                                  /*Change BoRrOw to burrow*/
	loans.put(loan.getId(), loan);                                 /*Change LoAnS to loans*/       /*Change GeT_ID to getId*/
	currentLoans.put(book.getId(), loan);                          /*Change gEtId to getId*/        /*Change Current_loans to currentLoans*/
	return loan;
    }
	
    public Loan getLoanByBookId(int bookId) {                            /*Change GeT_LoAn_By_BoOkId to getLoanByBookId*/
	if (currentLoans.containsKey(bookId))                           /*Change Current_loans to currentLoans*/ 
            return currentLoans.get(bookId);                        /*Change Current_loans to currentLoans*/
	return null;
    }
	
    public double calculateOverDueFine(Loan loan) {                      /*Change LoAn to loan */        /*Change CaLcUlAtE_OvEr_DuE_FiNe to calculateOverDueFine*/
	if (loan.isOverDue()) {                                       /*Change loan.Is_OvEr_DuE to loan.isOverDue*/
            long daysOverDue = Calendar.getInstance().getDaysDifference(loan.getDueDate());               /*Change GeTiNsTaNcE to getInstance*/       /*Change DaYs_OvEr_DuE to daysOverDue*/  /*Change GeT_DaYs_DiFfErEnCe to getDaysDifference*/     /*Change LoAn.GeT_DuE_DaTe to loan.getDueDate*/
            double fine = daysOverDue * FINEPERDAY;              /*Change FiNe_PeR_DaY to FINEPERDAY*/                          /*Change fInE to fine*/                 /*Change DaYs_OvEr_DuE to daysOverDue*/
            return fine;                                            /*Change fInE to fine*/
	}
	return 0.0;		
    }

    public void dischargeLoan(Loan currentLoan, boolean isDamaged) {     /*Change DiScHaRgE_LoAn to dischargeLoan*/     /*Change cUrReNt_LoAn to currentLoan*/ /*Change iS_dAmAgEd to isDamaged*/
	Member member = currentLoan.getMember();                      /*Change gEt_MeMbEr to getMember*/             /*Change cUrReNt_LoAn to currentLoan*/
	Book book = currentLoan.getBook();                           /*Change gEt_BoOk to getBook*/                 /*Change cUrReNt_LoAn to currentLoan*/     /*Change bOoK to book*/
	double overDueFine = calculateOverDueFine(currentLoan);   /*Change CaLcUlAtE_OvEr_DuE_FiNe to calculateOverDueFine*/   /*Change cUrReNt_LoAn to currentLoan*/ /*Change oVeR_DuE_FiNe to overDueFine*/
	member.addFine(overDueFine);                                 /*Change oVeR_DuE_FiNe to overDueFine*/       /*Change mEmBeR.AdD_FiNe to member.addFine*/
	member.dischargeLoan(currentLoan);                             /*Change cUrReNt_LoAn to currentLoan*/     /*Change mEmBeR.dIsChArGeLoAn to member.dischargeLoan*/
	book.returnState(isDamaged);                                        /*Change iS_dAmAgEd to isDamaged*/         /*Change bOoK.ReTuRn to book.returnState*/
	if(isDamaged) {                                               /*Change iS_dAmAgEd to isDamaged*/
            member.addFine(DAMAGEFREE);                             /*Change mEmBeR.AdD_FiNe to member.addFine*/      /*Change damageFee to DAMAGEFREE*/
            damagedBooks.put(book.getId(), book);                  /*Change DaMaGeD_BoOkS to damagedBooks*/           /*Change gEtId to getId*/   /*Change bOoK to book*/
	}
	currentLoan.discharge();                                       /*Change cUrReNt_LoAn to currentLoan*/         /*Change DiScHaRgE to discharge*/
            currentLoans.remove(book.getId());                             /*Change gEtId to getId*/   /*Change bOoK to book*/  /*Change Current_loans to currentLoans*/
    }

    public void checkCurrentLoans() {                                     /*Change cHeCk_Current_loans to checkCurrentLoans */
	for (Loan loan : currentLoans.values())                        /*Change lOaN to loan*/      /*Change currentLoans*/
            loan.checkOverDue();                                  /*Change lOaN.cHeCk_OvEr_DuE to loan.checkOverDue*/
    }

    public void repairBook(Book currentBook) {                            /*Change RePaIr_BoOk to repairBook*/       /*Change cUrReNt_BoOk to currentBook*/
	if (damagedBooks.containsKey(currentBook.getId())) {                  /*Change DaMaGeD_BoOkS to damagedBooks*/           /*Change gEtId to getId*/   /*Change cUrReNt_BoOk to currentBook*/
            currentBook.repair();                                  /*Change cUrReNt_BoOk to currentBook*/   /*Change RePaIr to repair*/
            damagedBooks.remove(currentBook.getId());                     /*Change DaMaGeD_BoOkS to damagedBooks*/           /*Change gEtId to getId*/   /*Change cUrReNt_BoOk to currentBook*/
	}   
	else 
            throw new RuntimeException("Library: repairBook: book is not damaged");	
    }
}
