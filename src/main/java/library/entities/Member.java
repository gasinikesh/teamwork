package library.entities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Member implements Serializable {

	private String lastName;			/* Changed LaSt_NaMe to lastName*/	
	private String firstName;			/* Changed FiRsT_NaMe to firstName*/	
	private String emailAddress;		/* Changed EmAiL_AdDrEsS to emailAddress*/
	private int phoneNumber;			/* Changed PhOnE_NuMbEr to phoneNumber*/
	private int memberId;				/* Change MeMbEr_Id to memberId*/
	private double finesOwing;			/* Change FiNeS_OwInG to finesOwing*/
	
	private Map<Integer, Loan> currentLoans;  /* Change cUrReNt_lOaNs to currentLoans */

	
	public Member(String lastName, String firstName, String emailAddress, int phoneNumber, int memberId) {		/* Change lAsT_nAmE to lastName & fIrSt_nAmE to firstName & eMaIl_aDdReSs to emailAddress & pHoNe_nUmBeR to phoneNumber & mEmBeR_iD to memberId*/
		this.lastName = lastName;		/* Change lAsT_nAmE to lastName*/
		this.firstName = firstName;		/* Change fIrSt_nAmE to firstName*/
		this.emailAddress = emailAddress; /* Change eMaIl_aDdReSs to emailAddress*/
		this.phoneNumber = phoneNumber;	/* Change pHoNe_nUmBeR to phoneNumber*/
		this.memberId = memberId;			/* Change MeMbEr_Id to memberId & mEmBeR_iD to memberId*/
		
		this.currentLoans = new HashMap<>();	/* Change cUrReNt_lOaNs to currentLoans */
	}

	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Member:  ").append(memberId).append("\n")		/* Change MeMbEr_Id to memberId*/
		  .append("  Name:  ").append(lastName).append(", ").append(firstName).append("\n")
		  .append("  Email: ").append(emailAddress).append("\n")
		  .append("  Phone: ").append(phoneNumber)
		  .append("\n")
		  .append(String.format("  Fines Owed :  $%.2f", finesOwing))		/* Change FiNeS_OwInG to finesOwing*/
		  .append("\n");
		
		for (Loan loan : currentLoans.values()) {			/* Change cUrReNt_lOaNs to currentLoans & LoAn to Loan */
			sb.append(loan).append("\n");                   /* Change LoAn to loan*/
		}		  
		return sb.toString();
	}

	
	public int getId() {		/* Change GeT_ID to getId */
		return memberId;		/* Change MeMbEr_Id to memberId*/
	}

	
	public List<Loan> getLoans() {								/* Change GeT_LoAnS to getLoans */
		return new ArrayList<Loan>(currentLoans.values());		/* Change cUrReNt_lOaNs to currentLoans */
	}

	
	public int getNumberOfCurrentLoans() {				/* Change cUrReNt_lOaNs to currentLoans & gEt_nUmBeR_Of_CuRrEnT_LoAnS to getNumberOfCurrentLoans */
		return currentLoans.size();
	}

	
	public double finesOwned() {			/* Change FiNeS_OwEd to finesOwned */
		return finesOwing;					/* Change FiNeS_OwInG to finesOwing*/
	}

	
	public void takeOutLoan(Loan loan) {						/* Change method TaKe_OuT_LoAn to takeOutLoan & lOaN to loan */
		if (!currentLoans.containsKey(loan.getId())) 			/* Change cUrReNt_lOaNs to currentLoans & lOaN to loan & GeT_Id to getId*/
			currentLoans.put(loan.getId(), loan);				/* Change cUrReNt_lOaNs to currentLoans & lOaN to loan & GeT_Id to getId*/
		
		else 
			throw new RuntimeException("Duplicate loan added to member");
				
	}

	
	public String getLastName() {			/* Change method GeT_LaSt_NaMe to getLastName */
		return lastName;
	}

	
	public String getFirstName() {		/* Change method name GeT_FiRsT_NaMe to getFirstName */
		return firstName;
	}


	public void addFine(double fine) {	/* Change AdD_FiNe to addFine */
		finesOwing += fine;				/* Change FiNeS_OwInG to finesOwing*/
	}
	
	public double payFine(double amount) {  /* Change AmOuNt to amount & PaY_FiNe to payFine */
		if (amount < 0) 						/* Change AmOuNt to amount*/
			throw new RuntimeException("Member.payFine: amount must be positive");
		
		double change = 0;
		if (amount > finesOwing) {			/* Change FiNeS_OwInG to finesOwing & AmOuNt to amount*/
			change = amount - finesOwing;	/* Change FiNeS_OwInG to finesOwing & AmOuNt to amount*/
			finesOwing = 0;				/* Change FiNeS_OwInG to finesOwing */
		}
		else 
			finesOwing -= amount;			/* Change FiNeS_OwInG to finesOwing & AmOuNt to amount*/
		
		return change;
	}


	public void dischargeLoan(Loan loan) {						/* Change LoAn to loan & dIsChArGeLoAn to dischargeLoan */
		if (currentLoans.containsKey(loan.getId())) 			/* Change cUrReNt_lOaNs to currentLoans & LoAn to loan & GeT_Id to getId */
			currentLoans.remove(loan.getId());				/* Change cUrReNt_lOaNs to currentLoans & LoAn to loan & GeT_Id to getId */
		
		else 
			throw new RuntimeException("No such loan held by member");
				
	}

}