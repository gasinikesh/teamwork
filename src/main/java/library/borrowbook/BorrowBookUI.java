package library.borrowbook;
import java.util.Scanner;


public class BorrowBookUI {
	
	public static enum UiState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED }; /*Change uI_STaTe to UiState*/

	private BorrowBookControl control;            /*Change bORROW_bOOK_cONTROL to BorrowBookControl -Mikesh*/       /*Change CoNtRoL to control -Mikesh*/
	private Scanner input;                        /*Change InPuT to input -Mikesh*/
	private UiState state;                        /*Change uI_STaTe to UiState -Mikesh*/    /*Change StaTe to state -Mikesh*/

	
	public BorrowBookUI(BorrowBookControl control) {        /*Change bORROW_bOOK_cONTROL to BorrowBookControl -Mikesh*/
		this.control = control;                         /*Change CoNtRoL to control -Mikesh*/
		input = new Scanner(System.in);                 /*Change InPuT to input*/
		state = UiState.INITIALISED;                    /*Change uI_STaTe to UiState*/  /*Change StaTe to state*/
		control.setUi(this);                            /*Change SeT_Ui to setUi*/
	}

	
	private String input(String prompt) {                   /*Change InPuT to input -Mikesh*/   /*Change PrOmPt to prompt -Mikesh*/
		System.out.print(prompt);                       /*Change PrOmPt to prompt -Mikesh*/
		return input.nextLine();                        /*Chnage InPuT to input -Mikesh*/
	}	
		
		
	private void output(Object object) {                    /*Change OuTpUt to output -Mikesh*/     /*Change ObJeCt to object -Mikesh*/
		System.out.println(object);                     /*Change ObJeCt to object -Mikesh*/
	}
	
			
	public void setState(UiState state) {                  /*Change uI_STaTe to UiState - Mikesh*/  /*Change SeT_StAtE to setState -Mikesh*/    /*Change StAtE to state -Mikesh*/
		this.state = state;                            /*Change StAtE to state -Mikesh*/
	}

	
	public void run() {                                    /*Change RuN to run -Mikesh*/
		output("Borrow Book Use Case UI\n");            /*Change OuTpUt to output -Mikesh*/
		
		while (true) {
			
			switch (state) {                        /*Change StAtE to state -Mikesh*/
			
			case CANCELLED:
				output("Borrowing Cancelled");      /*Change OuTpUt to output -Mikesh*/
				return;

				
			case READY:
				String memStr = input("Swipe member card (press <enter> to cancel): ");            /*Change InPuT to input -Mikesh*/    /*Change MEM_STR to memStr -Mikesh*/
				if (memStr.length() == 0) {                     /*Change MEM_STR to memStr -Mikesh*/
					control.cancel();                       /*Change CoNtRoL to control -Mikesh*/   /*Change CaNcEl to cancel -Mikesh*/
					break;
				}
				try {
					int memberId = Integer.valueOf(memStr).intValue();                 /*Change MEM_STR to memStr -Mikesh*/     /*Change MeMbEr_Id to memberId -Mikesh*/
					control.swiped(memberId);              /*Change CoNtRoL to control -Mikesh*/        /*Change MeMbEr_Id to memberId -Mikesh*/    /*Change SwIpEd to swiped -Mikesh*/
				}
				catch (NumberFormatException e) {
					output("Invalid Member Id");            /*Change OuTpUt to output -Mikesh*/
				}
				break;

				
			case RESTRICTED:
				input("Press <any key> to cancel");             /*Change iNpUT to input*/
				control.cancel();                               /*Change CoNtRoL to control -Mikesh*/   /*Change CaNcEl to cancel -Mikesh*/
				break;
			
				
			case SCANNING:
				String bookStringInput = input("Scan Book (<enter> completes): ");            /*Change iNpUT to input*/        /*Change BoOk_StRiNg_InPuT to bookStringInput -Mikesh */
				if (bookStringInput.length() == 0) {                /*Change BoOk_StRiNg_InPuT to bookStringInput -Mikesh*/
					control.complete();                     /*Change CoNtRoL to control -Mikesh*/   /*Change CoMpLeTe to complete -Mikesh*/
					break;
				}
				try {
					int bid = Integer.valueOf(bookStringInput).intValue();          /*Change BoOk_StRiNg_InPuT to bookStringInput -Mikesh*/     /*Change BiD to bid -Mikesh*/
					control.scanned(bid);                   /*Change CoNtRoL to control -Mikesh*/       /*Change BiD to bid -Mikesh*/   /*Change ScAnNeD to scanned -Mikesh*/
					
				} catch (NumberFormatException e) {
					output("Invalid Book Id");              /*Change OuTpUt to output -Mikesh*/
				} 
				break;
					
				
			case FINALISING:
				String ans = input("Commit loans? (Y/N): ");            /*Change iNpUT to input -Mikesh*/       /*Change AnS to ans -Mikesh*/
				if (ans.toUpperCase().equals("N")) {                    /*Change AnS to ans -Mikesh*/
					control.cancel();                       /*Change CoNtRoL to control -Mikesh*/   /*Change CaNcEl to cancel -Mikesh*/
					
				} else {
					control.commitLoans();                 /*Change CoNtRoL to control -Mikesh*/    /*Change CoMmIt_LoAnS to commitLoans -Mikesh*/
					input("Press <any key> to complete ");  /*Change iNpUT to input*/
				}
				break;
				
				
			case COMPLETED:
				output("Borrowing Completed");                  /*Change OuTpUt to output -Mikesh*/
				return;
	
				
			default:
				output("Unhandled state");                      /*Change OuTpUt to output -Mikesh*/
				throw new RuntimeException("BorrowBookUI : unhandled state :" + state);		/*Change StAtE to state -Mikesh*/	
			}
		}		
	}


	public void display(Object object) {                                    /*Change DiSpLaY to display -Mikesh*/
		output(object);                                                 /*Change OuTpUt to output -Mikesh*/
	}


}
