package library.borrowbook;
import java.util.ArrayList;
import java.util.List;

import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;

public class BorrowBookControl {                                        /*Change bORROW_bOOK_cONTROL to BorrowBookControl -Mikesh*/
	
	private BorrowBookUI uI;
	private Library library;                                        /*Change lIbRaRy to library -Mikesh*/
	private Member member;                                          /*Change mEmBeR to member -Mikesh*/
	private enum ControlState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };       /*Change CONTROL_STATE to ControlState -Mikesh*/
	private ControlState state;                                     /*Change CONTROL_STATE to ControlState -Mikesh*/    /*Change sTaTe to state -Mikesh*/
	private List<Book> pendingList;                                 /*Change pEnDiNg_LiSt to pendingList -Mikesh*/
	private List<Loan> completedList;                               /*Change cOmPlEtEd_LiSt to completedList -Mikesh*/
	private Book book;                                              /*Change bOoK to book -Mikesh*/
	
	
	public BorrowBookControl() {                                        /*Change bORROW_bOOK_cONTROL to BorrowBookControl -Mikesh*/
		this.library = library.getInstance();                       /*Change lIbRaRy to library -Mikesh*/   /*Change GeTiNsTaNcE to getInstance -Mikesh*/
		state = ControlState.INITIALISED;                           /*Change CONTROL_STATE to ControlState -Mikesh*/    /*Change sTaTe to state -Mikesh*/
	}
	

	public void setUi(BorrowBookUI Ui) {                                /*Change SeT_Ui to setUi -Mikesh*/
		if (!state.equals(ControlState.INITIALISED))                /*Change CONTROL_STATE to ControlState -Mikesh*/    /*Change sTaTe to state -Mikesh*/
			throw new RuntimeException("BorrowBookControl: cannot call setUI except in INITIALISED state");
			
		this.uI = Ui;
		Ui.setState(BorrowBookUI.UiState.READY);                   /*Change SeT_StAtE to setState -Mikesh*/ /*Change uI_STaTe to UiState -Mikesh*/
		state = ControlState.READY;                                 /*Change CONTROL_STATE to ControlState -Mikesh*/    /*Change sTaTe to state -Mikesh*/
	}

		
	public void swiped(int memberId) {                                 /*Change SwIpEd to swiped -Mikesh*/  /*Change mEmBeR_Id to memberId -Mikesh*/
		if (!state.equals(ControlState.READY))                      /*Change CONTROL_STATE to ControlState -Mikesh*/    /*Change sTaTe to state -Mikesh*/
			throw new RuntimeException("BorrowBookControl: cannot call cardSwiped except in READY state");
			
		member = library.getMember(memberId);                     /*Change lIbRaRy to library -Mikesh*/   /*Change mEmBeR to member -Mikesh*/  /*Change mEmBeR_Id to memberId -Mikesh*/ /*Change gEt_MeMbEr to getMember -Mikesh*/
		if (member == null) {                                       /*Change mEmBeR to member*/
			uI.display("Invalid memberId");                     /*Change DiSpLaY to display -Mikesh*/
			return;
		}
		if (library.canMemberBorrow(member)) {                      /*Change lIbRaRy to library -Mikesh*/   /*Change mEmBeR to member -Mikesh*/ /*Change cAn_MeMbEr_BoRrOw to canMemberBorrow -Mikesh*/
			pendingList = new ArrayList<>();                    /*Change pEnDiNg_LiSt to pendingList -Mikesh*/
			uI.setState(BorrowBookUI.UiState.SCANNING);        /*Change SeT_StAtE to setState -Mikesh*/ /*Change uI_STaTe to UiState*/
			state = ControlState.SCANNING;                      /*Change CONTROL_STATE to ControlState -Mikesh*/    /*Change sTaTe to state -Mikesh*/
		}
		else {
			uI.display("Member cannot borrow at this time");        /*Change DiSpLaY to display -Mikesh*/
			uI.setState(BorrowBookUI.UiState.RESTRICTED);      /*Change SeT_StAtE to setState -Mikesh*/ /*Change uI_STaTe to UiState*/
		}
	}
	
	
	public void scanned(int bookId) {                                   /*Change ScAnNeD to scanned -MIkesh*/   /*Change bOoKiD to bookId -Mikesh*/
		book = null;                                                /*Change bOoK to book -MIkesh*/
		if (!state.equals(ControlState.SCANNING))                   /*Change CONTROL_STATE to ControlState -Mikesh*/    /*Change sTaTe to state -Mikesh*/
			throw new RuntimeException("BorrowBookControl: cannot call bookScanned except in SCANNING state");
			
		book = library.getBook(bookId);                            /*Change lIbRaRy to library -Mikesh*/   /*Change bOoK to book -Mikesh*/ /*Change bOoKiD to bookId -Mikesh*/  /*Change gEt_BoOk to getBook -Mikesh*/
		if (book == null) {                                         /*Change bOoK to book -Mikesh*/
			uI.display("Invalid bookId");                       /*Change DiSpLaY to display -Mikesh*/
			return;
		}
		if (!book.isAvailable()) {                                 /*Change bOoK to book -Mikesh*/  /*Change iS_AvAiLaBlE to isAvalailable -Mikesh*/
			uI.display("Book cannot be borrowed");              /*Change DiSpLaY to display -Mikesh*/
			return;
		}
		pendingList.add(book);                                      /*Change pEnDiNg_LiSt to pendingList -Mikesh*/  /*Change bOoK to book -Mikesh*/
		for (Book B : pendingList)                                  /*Change pEnDiNg_LiSt to pendingList -Mikesh*/
			uI.display(B.toString());                           /*Change DiSpLaY to display -Mikesh*/
		
		if (library.getNumberOfLoansRemainingForMember(member) - pendingList.size() == 0) {              /*Change lIbRaRy to library -Mikesh*/   /*Change mEmBeR to member -Mikesh*/  /*Change pEnDiNg_LiSt to pendingList -Mikesh*/    /*Change gEt_NuMbEr_Of_LoAnS_ReMaInInG_FoR_MeMbEr to getNumberOfLoansRemainingForMember -Mikesh*/
			uI.display("Loan limit reached");                       /*Change DiSpLaY to display -Mikesh*/
			complete();                                             /*Change CoMpLeTe to complete -Mikesh*/
		}
	}
	
	
	public void complete() {                                            /*Change CoMpLeTe to complete -Mikesh*/
		if (pendingList.size() == 0)                                /*Change pEnDiNg_LiSt to pendingList -Mikesh*/
			cancel();                                           /*Change CaNcEl to cancel -Mikesh*/
		
		else {
			uI.display("\nFinal Borrowing List");               /*Change DiSpLaY too display -Mikesh*/
			for (Book book : pendingList)                       /*Change pEnDiNg_LiSt to pendingList -Mikesh*/  /*Change bOoK to book -Mikesh*/
				uI.display(book.toString());                    /*Change bOoK to book -Mikesh*/ /*Change DiSpLaY to display -Mikesh*/
			
			completedList = new ArrayList<Loan>();              /*Change cOmPlEtEd_LiSt to completedList -Mikesh*/
			uI.setState(BorrowBookUI.UiState.FINALISING);      /*Change SeT_StAtE to setState -Mikesh*/ /*Change uI_STaTe to UiState*/
			state = ControlState.FINALISING;                    /*Change CONTROL_STATE to ControlState -Mikesh*/    /*Change sTaTe to state -Mikesh*/
		}
	}


	public void commitLoans() {                                         /*Change CoMmIt_LoAnS to commitLoans -Mikesh*/
		if (!state.equals(ControlState.FINALISING))                 /*Change CONTROL_STATE to ControlState -Mikesh*/    /*Change sTaTe to state -Mikesh*/
			throw new RuntimeException("BorrowBookControl: cannot call commitLoans except in FINALISING state");
			
		for (Book B : pendingList) {                                   /*Change pEnDiNg_LiSt to pending List -Mikesh*/
			Loan loan = library.issueLoan(B, member);              /*Change lIbRaRy to library -Mikesh*/   /*Change mEmBeR to member -Mikesh*/ /*Change lOaN to loan -MIkesh*/  /*Change iSsUe_LoAn to issueLoan -Mikesh*/
			completedList.add(loan);                            /*Change cOmPlEtEd_LiSt to completedList -Mikesh*/  /*Change lOaN to loan -Mikesh*/
		}
		uI.display("Completed Loan Slip");                          /*Change DiSpLaY to display -Mikesh*/
		for (Loan loan : completedList)                             /*Change cOmPlEtEd_LiSt to completedList -Mikesh*/  /*Change lOaN to loan -Mikesh*/
			uI.display(loan.toString());                        /*Change DiSpLaY to display -Mikesh*/   /*Change lOaN to loan -Mikesh*/
		
		uI.setState(BorrowBookUI.UiState.COMPLETED);               /*Change SeT_StAtE to setState -Mikesh*/ /*Change uI_STaTe to UiState -Mikesh*/
		state = ControlState.COMPLETED;                             /*Change CONTROL_STATE to ControlState -Mikesh*/    /*Change sTaTe to state -MIkesh*/
	}

	
	public void cancel() {                                              /*Change CaNcEl to cancel -Mikesh*/
		uI.setState(BorrowBookUI.UiState.CANCELLED);               /*Change SeT_StAtE to setState -Mikesh*/ /*Change uI_STaTe to UiState -Mikesh*/
		state = ControlState.CANCELLED;                             /*Change CONTROL_STATE to ControlState -Mikesh*/    /*Change sTaTe to state -Mikesh*/
	}
	
	
}
