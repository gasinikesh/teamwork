package library.fixbook;
import java.util.Scanner;


public class FixBookUI {

	public static enum UiState { INITIALISED, READY, FIXING, COMPLETED }; /* Chnage enum uI_sTaTe to UiState*/

	private FixBookControl control;			/* Change fIX_bOOK_cONTROL to FixBookControl & CoNtRoL to control*/
	private Scanner input;					/* Change InPuT to input */
	private UiState state;						/* Chnage enum uI_sTaTe to UiState & StAtE to state*/

	
	public FixBookUI(FixBookControl control) {	/* Change fIX_bOOK_cONTROL to FixBookControl & CoNtRoL to control*/
		this.control = control;					/* Change  CoNtRoL to control */
		input = new Scanner(System.in);			/* Change InPuT to input */
		state = UiState.INITIALISED;			/* Chnage enum uI_sTaTe to UiState & StAtE to state*/
		control.setUi(this);					/* Change  CoNtRoL to control & SeT_Ui to setUi*/
	}


	public void setState(UiState state) {		/* Chnage enum uI_sTaTe to UiState & SeT_StAtE to setState*/
		this.state = state;						/* Change StAtE to state */
	}

	
	public void run() {							/* Change RuN to run */
		output("Fix Book Use Case UI\n");		/* Change OuTpUt to output */
		
		while (true) {
			
			switch (state) {				/* Change StAtE to state */
			
			case READY:
				String bookEntryString = input("Scan Book (<enter> completes): ");		/* Change BoOk_EnTrY_StRiNg to bookEntryString & iNpUt to input*/
				if (bookEntryString.length() == 0) 										/* Change BoOk_EnTrY_StRiNg to bookEntryString */
					control.scanningComplete();	/* Change  CoNtRoL to control & SCannING_COMplete to scanningComplete */
				
				else {
					try {
						int bookId = Integer.valueOf(bookEntryString).intValue();		/* Change BoOk_EnTrY_StRiNg to bookEntryString & BoOk_Id to bookId*/
						control.bookScanned(bookId);	/* Change  CoNtRoL to control & BoOk_Id to bookId & BoOk_ScAnNeD to bookScanned */
					}
					catch (NumberFormatException e) {
						output("Invalid bookId");		/* Change OuTpUt to output */
					}
				}
				break;	
				
			case FIXING:
				String ans = input("Fix Book? (Y/N) : ");		/* Change iNpUt to input & AnS to ans*/
				boolean fix = false;							/* Change FiX to fix */
				if (ans.toUpperCase().equals("Y")) 				/* Change AnS to ans */
					fix = true;									/* Change FiX to fix */
				
				control.fixBook(fix);		/* Change  CoNtRoL to control & FiX to fix & FiX_BoOk to fixBook*/
				break;
								
			case COMPLETED:
				output("Fixing process complete");		/* Change OuTpUt to output */
				return;
			
			default:
				output("Unhandled state");				/* Change OuTpUt to output */
				throw new RuntimeException("FixBookUI : unhandled state :" + state);	/* Change StAtE to state */		
			
			}		
		}
		
	}

	
	private String input(String prompt) {		/* Change iNpUt to input */
		System.out.print(prompt);
		return input.nextLine();				/* Change InPuT to input */
	}	
		
		
	private void output(Object object) {		/* Change OuTpUt to output */
		System.out.println(object);
	}
	

	public void display(Object object) {		/* Change dIsPlAy to display */
		output(object);
	}
	
	
}