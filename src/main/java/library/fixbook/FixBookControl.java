package library.fixbook;
import library.entities.Book;
import library.entities.Library;

public class FixBookControl {			/* Change class name fIX_bOOK_cONTROL to FixBookControl */
	
	private FixBookUI ui;				/* Change Ui to ui */
	private enum ControlState { INITIALISED, READY, FIXING }; /* Change enum CoNtRoL_StAtE to ControlState */
	private ControlState state;								/* Change enum CoNtRoL_StAtE to ControlState & StAtE to state*/
	
	private Library library;				/* Change LiBrArY to library*/
	private Book currentBook;				/* Change CuRrEnT_BoOk to currentBook*/


	public FixBookControl() {		/* Change fIX_bOOK_cONTROL to FixBookControl */
		this.library = Library.getInstance();				/* Change LiBrArY to library & GeTiNsTaNcE to getInstance */
		state = ControlState.INITIALISED;					/* Change enum CoNtRoL_StAtE to ControlState & StAtE to state */
	}
	
	
	public void setUi(FixBookUI ui) {					/* Change SeT_Ui to setUi */
		if (!state.equals(ControlState.INITIALISED)) 		/* Change enum CoNtRoL_StAtE to ControlState & StAtE to state */
			throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED state");
			
		this.ui = ui;							/* Change Ui to ui */
		ui.setState(FixBookUI.UiState.READY);		/* Change SeT_StAtE to setState & uI_sTaTe to UiState*/
		state = ControlState.READY;					/* Change enum CoNtRoL_StAtE to ControlState & StAtE to state */	
	}


	public void bookScanned(int bookId) {			/* Change method name BoOk_ScAnNeD to bookScanned & BoOkId to bookId*/
		if (!state.equals(ControlState.READY)) 		/* Change enum CoNtRoL_StAtE to ControlState & StAtE to state */
			throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY state");
			
		currentBook = library.getBook(bookId);	/* Change LiBrArY to library & CuRrEnT_BoOk to currentBook & gEt_BoOk to getBook & BoOkId to bookId*/
		
		if (currentBook == null) {				/* Change CuRrEnT_BoOk to currentBook */
			ui.display("Invalid bookId");		/* Change Ui to ui & dIsPlAy to display*/
			return;
		}
		if (!currentBook.isDamaged()) {		/* Change CuRrEnT_BoOk to currentBook & iS_DaMaGeD to isDamaged */
			ui.display("Book has not been damaged");	/* Change Ui to ui & dIsPlAy to display */
			return;
		}
		ui.display(currentBook.toString());		/* Change CuRrEnT_BoOk to currentBook & Ui to ui & dIsPlAy to display*/
		ui.setState(FixBookUI.UiState.FIXING);	/* Change Ui to ui & SeT_StAtE to setState & uI_sTaTe to UiState */
		state = ControlState.FIXING;				/* Change enum CoNtRoL_StAtE to ControlState & StAtE to state */	
	}


	public void fixBook(boolean mustFix) {		/* Change method name FiX_BoOk to fixBook & mUsT_FiX to mustFix*/
		if (!state.equals(ControlState.FIXING)) 		/* Change enum CoNtRoL_StAtE to ControlState & StAtE to state */
			throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING state");
			
		if (mustFix) 									/* Change arg mUsT_FiX to mustFix */
			library.repairBook(currentBook);			/* Change LiBrArY to library & CuRrEnT_BoOk to currentBook & RePaIr_BoOk to repairBook */
		
		currentBook = null;							/* Change CuRrEnT_BoOk to currentBook */
		ui.setState(FixBookUI.UiState.READY);		/* Change Ui to ui & SeT_StAtE to setState & uI_sTaTe to UiState */
		state = ControlState.READY;					/* Change enum CoNtRoL_StAtE to ControlState & StAtE to state */	
	}

	
	public void scanningComplete() {				/* Change method name SCannING_COMplete to scanningComplete */
		if (!state.equals(ControlState.READY)) 		/* Change enum CoNtRoL_StAtE to ControlState & StAtE to state */
			throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY state");
			
		ui.setState(FixBookUI.UiState.COMPLETED);		/* Change Ui to ui & SeT_StAtE to setState & uI_sTaTe to UiState */
	}

}
