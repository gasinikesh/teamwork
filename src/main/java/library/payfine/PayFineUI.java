package library.payfine;
import java.util.Scanner;


public class PayFineUI {


	public static enum UiState { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };		/* Change enum uI_sTaTe to UiState */

	private PayFineControl control;				/* Change pAY_fINE_cONTROL to PayFineControl & CoNtRoL to control */
	private Scanner input;
	private UiState state;							/* Change enum uI_sTaTe to UiState & StAtE to state */

	
	public PayFineUI(PayFineControl control) {	/* Change pAY_fINE_cONTROL to PayFineControl */
		this.control = control;					/* Change CoNtRoL to control */
		input = new Scanner(System.in);
		state = UiState.INITIALISED;				/* Change enum uI_sTaTe to UiState & StAtE to state */
		control.setUi(this);						/* Change SeT_uI to setUi */
	}
	
	
	public void setState(UiState state) {			/* Change enum uI_sTaTe to UiState & method SeT_StAtE to setState */
		this.state = state;							/* Change StAtE to state */
	}


	public void run() {						/* Change RuN to run */
		output("Pay Fine Use Case UI\n");
		
		while (true) {
			
			switch (state) {						/* Change StAtE to state */
			
			case READY:
				String memStr = input("Swipe member card (press <enter> to cancel): ");  /* Change Mem_Str to memStr */
				if (memStr.length() == 0) {											/* Change Mem_Str to memStr */
					control.cancel();				/* Change CoNtRoL to control & CaNcEl to cancel */
					break;
				}
				try {
					int memberId = Integer.valueOf(memStr).intValue();			/* Change Mem_Str to memStr & Member_ID to memberId*/
					control.cardSwiped(memberId);	/* Change CoNtRoL to control & Member_ID to memberId & CaRd_sWiPeD to cardSwiped */
				}
				catch (NumberFormatException e) {
					output("Invalid memberId");
				}
				break;
				
			case PAYING:
				double amount = 0;			/* Change AmouNT to amount */
				String amtStr = input("Enter amount (<Enter> cancels) : "); /* Change Amt_Str to amtStr */
				if (amtStr.length() == 0) {		/* Change Amt_Str to amtStr */
					control.cancel();				/* Change CoNtRoL to control & CaNcEl to cancel */
					break;
				}
				try {
					amount = Double.valueOf(amtStr).doubleValue();		/* Change AmouNT to amount & Amt_Str to amtStr */
				}
				catch (NumberFormatException e) {}
				if (amount <= 0) {						/* Change AmouNT to amount */
					output("Amount must be positive");
					break;
				}
				control.payFine(amount);			/* Change CoNtRoL to control & AmouNT to amount & PaY_FiNe to payFine */
				break;
								
			case CANCELLED:
				output("Pay Fine process cancelled");
				return;
			
			case COMPLETED:
				output("Pay Fine process complete");
				return;
			
			default:
				output("Unhandled state");
				throw new RuntimeException("FixBookUI : unhandled state :" + state);	/* Change StAtE to state */		
			
			}		
		}		
	}

	
	private String input(String prompt) {
		System.out.print(prompt);
		return input.nextLine();
	}	
		
		
	private void output(Object object) {
		System.out.println(object);
	}	
			

	public void display(Object object) {		/* Change DiSplAY to display */
		output(object);
	}


}
