package library.payfine;
import library.entities.Library;
import library.entities.Member;

public class PayFineControl {				/* change class name pAY_fINE_cONTROL to PayFineControl */
	
	private PayFineUI ui;					/* Change Ui to ui */
	private enum ControlState { INITIALISED, READY, PAYING, COMPLETED, CANCELLED }; /* Change cOnTrOl_sTaTe to ControlState */
	private ControlState state;			/* Change cOnTrOl_sTaTe to ControlState & StAtE to state */
	
	private Library library;			/* Change LiBrArY to library */
	private Member member;				/* Change MeMbEr to member */


	public PayFineControl() {				/* change pAY_fINE_cONTROL to PayFineControl */
		this.library = Library.getInstance();	/* Change LiBrArY to library & GeTiNsTaNcE to getInstance */
		state = ControlState.INITIALISED;	/* Change cOnTrOl_sTaTe to ControlState & StAtE to state */
	}
	
	
	public void setUi(PayFineUI ui) {			/* Change SeT_uI to setUi & uI to ui */
		if (!state.equals(ControlState.INITIALISED)) {		/* Change cOnTrOl_sTaTe to ControlState & StAtE to state */
			throw new RuntimeException("PayFineControl: cannot call setUI except in INITIALISED state");
		}	
		this.ui = ui;							/* Change uI to ui */
		ui.setState(PayFineUI.UiState.READY);		/* Change uI to ui & SeT_StAtE to setState & uI_sTaTe to UiState */
		state = ControlState.READY;			/* Change cOnTrOl_sTaTe to ControlState & StAtE to state */	
	}


	public void cardSwiped(int memberId) {	/* Change CaRd_sWiPeD to cardSwiped & MeMbEr_Id to memberId */
		if (!state.equals(ControlState.READY)) 	/* Change cOnTrOl_sTaTe to ControlState & StAtE to state*/
			throw new RuntimeException("PayFineControl: cannot call cardSwiped except in READY state");
			
		member = library.getMember(memberId);	/* Change LiBrArY to library & MeMbEr to member & MeMbEr_Id & gEt_MeMbEr to getMember */
		
		if (member == null) {					/* Change MeMbEr to member */
			ui.display("Invalid Member Id");	/* Change Ui to ui & DiSplAY to display */
			return;
		}
		ui.display(member.toString());			/* Change MeMbEr to member & Ui to ui & DiSplAY to display */
		ui.setState(PayFineUI.UiState.PAYING);	/* Change SeT_StAtE to setState & uI_sTaTe to uiState & Ui to Ui */
		state = ControlState.PAYING;				/* Change cOnTrOl_sTaTe to ControlState & StAtE to state */
	}
	
	
	public void cancel() {							/*  Change CaNcEl to cancel */
		ui.setState(PayFineUI.UiState.CANCELLED);		/* Change SeT_StAtE to setState & uI_sTaTe to UiState & Ui to ui */
		state = ControlState.CANCELLED;			/* Change cOnTrOl_sTaTe to ControlState & StAtE to state */
	}


	public double payFine(double amount ) {				/* Change PaY_FiNe to payFine & AmOuNt to amount */
		if (!state.equals(ControlState.PAYING)) 		/* Change cOnTrOl_sTaTe to ControlState & StAtE to state */
			throw new RuntimeException("PayFineControl: cannot call payFine except in PAYING state");
			
		double change = member.payFine(amount );		/* Change MeMbEr to member & PaY_FiNe to payFine & AmOuNt to amount & ChAnGe to change */
		if (change > 0) 								/* Change ChAnGe to change */
			ui.display(String.format("Change: $%.2f", change)); /* Change DiSplAY to display & ChAnGe to change */
		
		ui.display(member.toString());					/* Change MeMbEr to member & DiSplAY to display */
		ui.setState(PayFineUI.UiState.COMPLETED);		/* Change SeT_StAtE to setState & uI_sTaTe to UiState & Ui to ui */
		state = ControlState.COMPLETED;			/* Change cOnTrOl_sTaTe to ControlState & StAtE to state & Ui to ui */
		return change;							/* Change ChAnGe to change */
	}
	


}
