package library;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import library.borrowbook.BorrowBookUI;
import library.borrowbook.BorrowBookControl;
import library.entities.Book;
import library.entities.Calendar;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;
import library.fixbook.FixBookControl;
import library.fixbook.FixBookUI;
import library.payfine.PayFineUI;
import library.payfine.PayFineControl;
import library.returnBook.ReturnBookUI;
import library.returnBook.ReturnBookControl;

public class Main {
    
    private static Scanner input;  /* Change IN to input --Nikesh */
    private static Library library;     /* Change LIB to library --Nikesh */
    private static String menu;     /* Change MENU to menu --Nikesh */
    private static Calendar calender;        /* Change CAL to calender --Nikesh */
    private static SimpleDateFormat simpleDateFormat;    /* Change SDF to simpleDateFormat --Nikesh */

    private static String getMenu() {      /* Change Get_menu to getMenu --Nikesh */
        StringBuilder stringBuilder = new StringBuilder();     /* Change sb to stringBuilder --Nikesh */
        stringBuilder.append("\nLibrary Main Menu\n\n")    /* Change sb to stringBuilder --Nikesh */
          .append("  M  : add member\n")
          .append("  LM : list members\n")
          .append("\n")
          .append("  B  : add book\n")
          .append("  LB : list books\n")
          .append("  FB : fix books\n")
          .append("\n")
          .append("  L  : take out a loan\n")
          .append("  R  : return a loan\n")
          .append("  LL : list loans\n")
          .append("\n")
          .append("  P  : pay fine\n")
          .append("\n")
          .append("  T  : increment date\n")
          .append("  Q  : quit\n")
          .append("\n")
          .append("Choice : ");

        return stringBuilder.toString();    /* Change sb to stringBuilder --Nikesh */
    }

    public static void main(String[] args) {		
        try {			
            input = new Scanner(System.in);    /* Change IN to input --Nikesh */
            library = Library.getInstance();    /* Change LIB to library and GeTiNsTaNcE to getInstance --Nikesh */
            calender = Calendar.getInstance();   /* Change CAL to calender --Nikesh */
            simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");   /* Change SDF to simpleDateFormat --Nikesh */

            for (Member m : library.listMembers()) {   /* Change LIB to library and lIsT_MeMbErS to listMembers --Nikesh */
                    output(m);
            }
            output(" ");
            
            for (Book b : library.listBooks()) {   /* Change LIB to library and lIsT_BoOkS to listBooks --Nikesh */
                    output(b);
            }			
            menu = getMenu();  /* Change Change MENU to menu and Get_menu to getMenu --Nikesh */
            boolean e = false;
            
            while (!e) {
                output("\n" + simpleDateFormat.format(calender.getDate()));  /* Change SDF to simpleDateFormat, CAL to calender gEt_DaTe to getDate --Nikesh */
                String c = input(menu); /* Change MENU to menu */
                switch (c.toUpperCase()) {
                    case "M": 
                        addMember();   /* Change ADD_MEMBER to addMember --Nikesh */
                        break;

                    case "LM": 
                        listMembers();     /* Change LIST_MEMBERS to listMembers --Nikesh */
                        break;

                    case "B": 
                        addBook();     /* Change ADD_BOOK to addBook --Nikesh */
                        break;

                    case "LB": 
                        listBooks();       /* Change LIST_BOOKS to listBooks --Nikesh */
                        break;

                    case "FB": 
                        fixBooks();     /* Change FIX_BOOKS to fixBooks --Nikesh */
                        break;

                    case "L": 
                        borrowBook();  /* Change BORROW_BOOK to borrowBook --Nikesh */
                        break;

                    case "R": 
                        returnBook();  /* Change RETURN_BOOK to returnBook --Nikesh */
                        break;

                    case "LL": 
                        listCurrentLoans();   /* Change LIST_CURRENT_LOANS to listCurrentLoans --Nikesh */
                        break;

                    case "P": 
                        payFines();        /* Change PAY_FINES to payFines --Nikesh */
                        break;

                    case "T": 
                        incrementDate();   /* Change INCREMENT_DATE to incrementDate --Nikesh */
                        break;

                    case "Q": 
                        e = true;
                        break;

                    default: 
                        output("\nInvalid option\n");
                        break;
                }
                Library.save(); /* Change SaVe to save --Nikesh */
            }			
        } catch (RuntimeException e) {
                output(e);
        }		
        output("\nEnded\n");
    }	


    private static void payFines() {   /* Change PAY_FINES to payFines --Nikesh */
        new PayFineUI(new PayFineControl()).run();        /* Change RuN to run pAY_fINE_cONTROL to PayFineControl --Nikesh */	
    }


    private static void listCurrentLoans() {  /* Change LIST_CURRENT_LOANS to listCurrentLoans --Nikesh */
        output("");
        for (Loan loan : library.listCurrentLoans()) {    /* Change  Change LIB to library andlISt_CuRrEnT_LoAnS to listCurrentLoans --Nikesh */
                output(loan + "\n");
        }		
    }

    private static void listBooks() {  /* Change LIST_BOOKS to listBooks --Nikesh */
        output("");
        for (Book book : library.listBooks()) {    /* Change Change LIB to library and lIsT_BoOkS to listBooks --Nikesh */
                output(book + "\n");
        }		
    }

    private static void listMembers() {    /* Change LIST_MEMBERS to listMembers --Nikesh */
        output("");
        for (Member member : library.listMembers()) {      /* Change Change LIB to library and lIsT_MeMbErS to listMembers --Nikesh */
                output(member + "\n");
        }		
    }

    private static void borrowBook() {     /* Change BORROW_BOOK to borrwoBook --Nikesh */   
        new BorrowBookUI(new BorrowBookControl()).run();  	/* Change bORROW_bOOK_cONTROL to BorrowBookControl and RuN to run --Nikesh */   	
    }

    private static void returnBook() {     /* Change RETURN_BOOK to returnBook --Nikesh */
        new ReturnBookUI(new ReturnBookControl()).run();    /* Change RuN to run --Nikesh */   		
    }

    private static void fixBooks() {   /* Change FIX_BOOKS to fixBooks --Nikesh */
        new FixBookUI(new FixBookControl()).run();        /* Change fIX_bOOK_cONTROL to FixBookControl and RuN to run --Nikesh */	
    }

    private static void incrementDate() {  /* Change INCREMENT_DATE to incrementDate --Nikesh */
        try {
            int days = Integer.valueOf(input("Enter number of days: ")).intValue();
            calender.incrementDate(days);   /* Change CAL to calender */
            library.checkCurrentLoans();      /* Change Change LIB to library and cHeCk_CuRrEnT_LoAnS to checkCurrentLoans --Nikesh */
            output(simpleDateFormat.format(calender.getDate())); /* Change SDF to simpleDateFormat CAL to calender gEt_DaTe to getDate --Nikesh */

        } catch (NumberFormatException e) {
            output("\nInvalid number of days\n");
        }
    }

    private static void addBook() {        /* Change ADD_BOOK to addBook --Nikesh */
        String author = input("Enter author: ");        /* Change AuThOr to author --Nikesh */
        String title  = input("Enter title: ");     /* Change TiTlE to title --Nikesh */
        String callNumber = input("Enter call number: ");  /* Change CaLl_NuMbEr to callNumber --Nikesh */
        Book book = library.addBook(author, title, callNumber);   /* Change Change LIB to library, BoOk to book, aDd_BoOk to addBook, AuThOr to author, TiTlE to title and CaLl_NuMbEr to callNumber --Nikesh */
        output("\n" + book + "\n"); /* Change BoOk to book */
    }

    private static void addMember() {  /* Change ADD_MEMBER to addMember --Nikesh */
        try {
            String lastName = input("Enter last name: ");  /* Change LaSt_NaMe to lastName --Nikesh */
            String firstName  = input("Enter first name: ");   /* Change FiRsT_NaMe to firstName --Nikesh */
            String emailAddress = input("Enter email address: ");      /* Change EmAiL_AdDrEsS to emailAddress --Nikesh */
            int phoneNumber = Integer.valueOf(input("Enter phone number: ")).intValue();   /* Change PhOnE_NuMbEr to phoneNumber --Nikesh */
            Member member = library.addMember(lastName, firstName, emailAddress, phoneNumber);     /* Change Change LIB to library, MeMbEr to member, aDd_MeMbEr to addMember, LaSt_NaMe to lastName, FiRsT_NaMe to firstName, EmAiL_AdDrEsS to emailAddress and PhOnE_NuMbEr to phoneNumber --Nikesh */
            output("\n" + member + "\n");   /* Change MeMbEr to member --Nikesh */
        } catch (NumberFormatException e) {
            output("\nInvalid phone number\n");
        }
    }

    private static String input(String prompt) {
        System.out.print(prompt);
        return input.nextLine();    /*Change IN to input*/
    }

    private static void output(Object object) {
        System.out.println(object);
    }
}
