package library.returnBook;
import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;

public class ReturnBookControl {

    private ReturnBookUI uI;    /* Change Ui to uI --Nikesh*/
    private enum ControlState { INITIALISED, READY, INSPECTING };  /* Change cOnTrOl_sTaTe to ControlState  --Nikesh*/
    private ControlState state;     /* Change cOnTrOl_sTaTe to ControlState and sTaTe to state  --Nikesh*/
    private Library library;        /* Change lIbRaRy to library --Nikesh*/
    private Loan currentLoan;      /* Change CurrENT_loan to currentLoan --Nikesh*/

    public ReturnBookControl() {
        this.library = Library.getInstance();   /* Change lIbRaRy to library and GeTiNsTaNcE to getInstance --Nikesh*/
        state = ControlState.INITIALISED;      /* Change cOnTrOl_sTaTe to ControlState and sTaTe to state  --Nikesh*/
    }

    public void setUI(ReturnBookUI uI) {   /* Change sEt_uI to setUI --Nikesh*/
        if (!state.equals(ControlState.INITIALISED)) {   /* Change cOnTrOl_sTaTe to ControlState and sTaTe to state  --Nikesh*/ 
            throw new RuntimeException("ReturnBookControl: cannot call setUI except in INITIALISED state");	
        }
        this.uI = uI;   /* Change Ui to uI --Nikesh*/
        uI.setState(ReturnBookUI.UIState.READY);  /* Change uIState to UIState --Nikesh*/
        state = ControlState.READY;    /* Change cOnTrOl_sTaTe to ControlState and sTaTe to state  --Nikesh*/ 		
    }

    public void bookScanned(int bookId) {     /* Change bOoK_sCaNnEd to bookScanned and bOoK_iD to bookId --Nikesh*/
        if (!state.equals(ControlState.READY)) {     /* Change cOnTrOl_sTaTe to ControlState and sTaTe to state  --Nikesh*/
            throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY state");
        }
        Book currentBook = library.getBook(bookId);  /* Change cUrReNt_bOoK to currentBook, lIbRaRy to library, gEt_BoOk to getBook and bOoK_iD to bookId --Nikesh*/

        if (currentBook == null) {     /* Change cUrReNt_bOoK to currentBook */
            uI.display("Invalid Book Id");  /* Change Ui to uI and DiSpLaY to display --Nikesh */
            return;
        }
        
        if (!currentBook.isOnLoan()) {   /* Change cUrReNt_bOoK to currentBook and iS_On_LoAn to isOnLoan */
            uI.display("Book has not been borrowed");   /* Change Ui to uI and DiSpLaY to display --Nikesh */
            return;
        }	
        
        currentLoan = library.getLoanByBookId(bookId);	/* Change CurrENT_loan to currentLoan, lIbRaRy to library, GeT_LoAn_By_BoOkId to getLoanByBookId and bOoK_iD to bookId  --Nikesh*/
        double overDueFine = 0.0;     /* Change Over_Due_Fine to overDueFine --Nikesh*/
        
        if (currentLoan.isOverDue()) {   /* Change CurrENT_loan to currentLoan and Is_OvEr_DuE to isOverDue --Nikesh*/
            overDueFine = library.calculateOverDueFine(currentLoan);  /* Changes Over_Due_Fine to overDueFine, lIbRaRy to library, CaLcUlAtE_OvEr_DuE_FiNe to calculateOverDueFine and CurrENT_loan to currentLoan --Nikesh*/
        }

        uI.display("Inspecting");    /* Change Ui to uI and DiSpLaY to display --Nikesh */
        uI.display(currentBook.toString());    /* Changes Ui to uI, cUrReNt_bOoK to currentBook and DiSpLaY to display --Nikesh */
        uI.display(currentLoan.toString());    /* Changes Ui to uI, CurrENT_loan to currentLoan and DiSpLaY to display --Nikesh */

        if (currentLoan.isOverDue()) {   /* Change CurrENT_loan to currentLoan and Is_OvEr_DuE to isOverDue --Nikesh */
            uI.display(String.format("\nOverdue fine : $%.2f", overDueFine));     /* Changes Ui to uI, Over_Due_Fine to overDueFine and DiSpLaY to display --Nikesh */
        }
        uI.setState(ReturnBookUI.UIState.INSPECTING); /* Change uIState to UIState --Nikesh*/
        state = ControlState.INSPECTING;      /* Changes cOnTrOl_sTaTe to ControlState and sTaTe to state  --Nikesh*/	
    }

    public void scanningComplete() {   /* Change sCaNnInG_cOmPlEtE to scanningComplete --Nikesh */
        if (!state.equals(ControlState.READY)) {   /* Changes cOnTrOl_sTaTe to ControlState and sTaTe to state  --Nikesh*/
            throw new RuntimeException("ReturnBookControl: cannot call scanningComplete except in READY state");
        }
        uI.setState(ReturnBookUI.UIState.COMPLETED);       /* Change uIState to UIState --Nikesh*/
    }

    public void dischargeLoan(boolean isDamaged) {    /* Changes dIsChArGe_lOaN to dischargeLoan and iS_dAmAgEd to isDamaged  --Nikesh */
        if (!state.equals(ControlState.INSPECTING)) {    /* Changes cOnTrOl_sTaTe to ControlState and sTaTe to state  --Nikesh*/
            throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING state");
        }
        library.dischargeLoan(currentLoan, isDamaged);   /* Changes lIbRaRy to library, CurrENT_loan to currentLoan, dIsChArGe_lOaN to dischargeLoan and iS_dAmAgEd to isDamaged  --Nikesh */
        currentLoan = null;    /* Change CurrENT_loan to currentLoan --Nikesh */
        uI.setState(ReturnBookUI.UIState.READY);  /* Change uIState to UIState --Nikesh*/
        state = ControlState.READY;        /* Changes cOnTrOl_sTaTe to ControlState and sTaTe to state  --Nikesh*/			
    }
}
