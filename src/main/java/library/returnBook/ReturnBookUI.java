package library.returnBook;
import java.util.Scanner;

public class ReturnBookUI {

    public static enum UIState { INITIALISED, READY, INSPECTING, COMPLETED };   /* Change uI_sTaTe to UIState --Nikesh*/
    private ReturnBookControl returnBookControl;  /* Change control to returnBookControl --Nikesh*/
    private Scanner scanner;  /* Change iNpUt to scanner --Nikesh*/
    private UIState uIState;     /* Change uI_sTaTe to UIState and StATe uIState --Nikesh*/

    public ReturnBookUI(ReturnBookControl returnBookControl) {    /* Change cOnTrOL to returnBookControl --Nikesh*/
        this.returnBookControl = returnBookControl;     /* Changes CoNtRoL to returnBookControl cOnTrOL to returnBookControl --Nikesh*/
        scanner = new Scanner(System.in); /* Change iNpUt to scanner --Nikesh*/
        uIState = UIState.INITIALISED;   /* Change uI_sTaTe to UIState and StATe uIState --Nikesh*/
        returnBookControl.setUI(this);   /* changes cOnTrOL to returnBookControl and sEt_uI to setUI --Nikesh*/
    }

    public void run() {		/* Change RuN to run --Nikesh*/
        output("Return Book Use Case UI\n");    /* Change oUtPuT to output --Nikesh*/

        while (true) {      
            switch (uIState) {    /* change StATe to uIState --Nikesh*/
                case INITIALISED:
                    break;
                case READY:
                    String bookInputString = input("Scan Book (<enter> completes): ");    /* change BoOk_InPuT_StRiNg to bookInputString and iNpUt to input --Nikesh*/
                    if (bookInputString.length() == 0) {  /* change BoOk_InPuT_StRiNg to bookInputString --Nikesh */
                        returnBookControl.scanningComplete();    /* Changes CoNtRoL to returnBookControl and sCaNnInG_cOmPlEtE to scanningComplete --Nikesh*/
                    } else {
                        try {
                            int bookId = Integer.valueOf(bookInputString).intValue();     /* Changes Book_Id to bookId and BoOk_InPuT_StRiNg to bookInputString --Nikesh*/
                            returnBookControl.bookScanned(bookId);      /* Changes Book_Id to bookId, CoNtRoL to returnBookControl and bOoK_sCaNnEd to bookScanned --Nikesh */
                        }
                        catch (NumberFormatException e) {
                            output("Invalid bookId");       /* Change oUtPuT to output --Nikesh*/
                        }					
                    }
                    break;				
                case INSPECTING:
                    String ans = input("Is book damaged? (Y/N): "); /* change AnS to ans and iNpUt to input --Nikesh*/
                    boolean isDamaged = false;     /* Change Is_DAmAgEd to isDamaged --Nikesh*/
                    if (ans.toUpperCase().equals("Y")) {            /* change AnS to ans --Nikesh */			
                        isDamaged = true;  /* Change Is_DAmAgEd to isDamaged --Nikesh*/
                    }
                    returnBookControl.dischargeLoan(isDamaged);     /* Changes CoNtRoL to returnBookControl, dIsChArGe_lOaN to dischargeLoan and Is_DAmAgEd to isDamaged --Nikesh*/
                case COMPLETED:
                    output("Return processing complete");   /* Change oUtPuT to output --Nikesh*/
                    return;
                default:
                    output("Unhandled state");      /* Change oUtPuT to output --Nikesh*/
                    throw new RuntimeException("ReturnBookUI : unhandled state :" + uIState); /* Change StATe uIState --Nikesh*/			
            }
        }
    }

    private String input(String prompt) {   /* Change iNpUt to input and PrOmPt to prompt --Nikesh*/
        System.out.print(prompt);   /* Change PrOmPt to prompt --Nikesh*/
        return scanner.nextLine();        /* Change iNpUt to scanner --Nikesh*/
    }		

    private void output(Object object) {    /* Change ObJeCt to object --Nikesh*/
        System.out.println(object);     /* Change ObJeCt to object --Nikesh*/
    }

    public void display(Object object) {    /* Changes DiSpLaY to display and ObJeCt to object --Nikesh*/
        output(object);     /* Change oUtPuT to output ObJeCt to object --Nikesh*/
    }

    public void setState(UIState uIState) {      /* Change sEt_sTaTe to setState,uI_sTaTe to UIState and state to uIState  --Nikesh*/
        this.uIState = uIState;     /* Change StATe to uIState and state to uIState  --Nikesh*/
    }

}
